# Reference: <https://postmarketos.org/devicepkg>
# Contributor: Mis012 <michael.srba@seznam.cz>
# Contributor: Minecrell <minecrell@minecrell.net>
pkgname="device-asus-me176c"
pkgdesc="ASUS MeMO Pad 7 (ME176C(X))"
pkgver=1
pkgrel=5
url="https://postmarketos.org"
license="MIT"
arch="x86_64"
options="!check !archcheck"
depends="postmarketos-base linux-asus-me176c firmware-asus-me176c-acpi intel-ucode $pkgname-factory mesa-dri-classic mesa-vulkan-intel"
makedepends="devicepkg-dev meson eudev-dev"

_commit=3155254999ac36c3051a2118c415de25a072c0f6
source="
	deviceinfo
	https://github.com/me176c-dev/linux-me176c/archive/$_commit.tar.gz
"
subpackages="
	$pkgname-factory
	$pkgname-nonfree-firmware:nonfree_firmware
"

build() {
	devicepkg_build $startdir $pkgname

	meson \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--buildtype=release \
		-Dsystemd=false -Dopenrc=true \
		linux-me176c-$_commit/factory build-factory
	ninja -C build-factory
}

package() {
	devicepkg_package $startdir $pkgname
}

factory() {
	pkgdesc="WiFi/BT MAC address loader"
	depends="bluez-btmgmt"
	DESTDIR="$subpkgdir" ninja -C "$srcdir"/build-factory install
}

nonfree_firmware() {
	pkgdesc="WiFi, Bluetooth and Sound firmware"
	depends="linux-firmware-intel firmware-asus-me176c"
	mkdir "$subpkgdir"
}

sha512sums="8fb5d58113a9180ecf6d52087e2f71820d3a2f968aa43eb18b80fd999e66e5f4c224bd7048654bd3a51e6b6ed4e6baca3325dcdde14de0e0c5720326603d8d98  deviceinfo
75243e3d0bc9a352465683ec8cf8b83566e4131bb7fae0f388267a62bdbf4691c955ed034b30df550f921b771080688123ae8db144b72f786bf1677ef1d83f57  3155254999ac36c3051a2118c415de25a072c0f6.tar.gz"
